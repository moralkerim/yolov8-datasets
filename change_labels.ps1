# Specify the directory where your .txt files are located
$txtDirectory = "C:\Users\Kerim\Desktop\Terrain-eye.v1i.yolov8-obb\terrain_eye_valid\labels"


# Get all .txt files in the directory
$txtFiles = Get-ChildItem -Path $txtDirectory -Filter *.txt

# Iterate over each .txt file
foreach ($file in $txtFiles) {
    # Read the content of the file
    $content = Get-Content $file.FullName

    # Iterate over each line and replace the first number with "10"
    $modifiedContent = $content | ForEach-Object {
        $_ -replace '^(\d+)', '10'
    }

    # Write the modified content back to the file
    Set-Content -Path $file.FullName -Value $modifiedContent
}

Write-Host "Conversion completed successfully."
